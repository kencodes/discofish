Changes made without branching... oops :(
1) Changed both guest and user swipe/login to use toggle_user_session()
    - Badge_id is now requested in main loop of the program.
    - This might take care of guest button debounce problem, too
2) Use mySQLdb cp_reconnect=True to reestablish SQL db connection???
    - TRY THIS: http://stackoverflow.com/questions/207981/how-to-enable-mysql-client-auto-re-connect-with-mysqldb
    - ALSO check mySQL options "interactive_timeout" and "wait_timeout"
    - Ensure auto-reconnect is enabled
        - Issue command again, this should cause a reconnect
    - Ensure I'm not closing the connection
    - Consider using stored SQL procedures where quick response is desired (add dollar, etc.)


Useful info!


verbosity is the debug level:
    verbosity=0 : No debug prints
    verbosity=1 : Some debug prints
    verbosity=2 : MOARR debug prints
    ...         : so far, we only crank it up to two



BUGS [O]=Open; [F]=Fixed
[O] Need better way to quickly update SQL with new balance
[O] Guest balance not printing to terminal correctly when guest first logs in
    * Phil's webpage looks correct though, so maybe not getting data from SQL?
    * If guest times out with incorrect balance showing in terminal, that wrong balance is then written to SQL!
[O] guest has zero balance always runs, need to log them out and stop checking their balance
[O] make guest a user that's logged in if balance > 0, but they log out if someone badges in
[O] Make guest logged in whenever their balance > 0
[O] Give guest access to taps with "ALL" access
[O] May need arduino or similar to do pulse counting on flow meter
[F] Need to update MySQL tables with new volumes, balances...
[F] People without tap access can still increment flow counters...



TO DO:
* Must get rid of user_list and tap_list and only get data from SQL
    * I keep having conflict problems where data is updated in one place and not the other
* Make separate button for guest login
    * Swipe badge or press here for guest login
    * the button could cause a normal login using user_id=1 for guest
    * Everything would be the same for guest and normal login from that point including reports
* If taps are opened and flow counters register flow within 1 second, shut off taps because someone may have left the tap open when the beer stopped pouring
* In the event of database unavailable, log user in as EMERGENCY_BEER
    * E_B has $0.00 balance and can only pour beer if balance is positive
    * User must charge up balance with bill acceptor before pouring
    * When Balance is positive, valve opens and flow meter tracks tap/amount poured
    * email admins with amount poured per tap so we can manually update the keg table
* Changing pricing depending on employee statsus?
* Rating system
    * If user drinks more than X oz, they get emailed a google doc Form
    * They rate the beer, goes into google doc spreadsheet which MySQL can maybe access?
        * Maybe python can access it?
    * Beers are displayed with average rating and number of ratings
* Generate user_list from MySQL table
* Generate tap_list from MySQL table
* Save all data to MySQL tables
* Advertise new beer - Email everyone with access when a new beer is put on tap
* log_session(??) - Takes current session info and formats it for the comprehensive log file
* auto logout if user forgets (1 minute?)



Add features:
* Badge/NFC reader, associate ID numbers with user accounts
* add snarky messages based on amount poured, time poured, etc.








