######################################################################
## User class

class User(object):
    """a structure class for users"""
    def __init__(self, user_id, first_name, last_name, is_employee, is_admin, balance, email, badge_id, nfc_id):
        self.user_id = user_id
        self.first_name = first_name
        self.last_name = last_name
        self.is_employee = is_employee
        self.is_admin = is_admin
        self.balance = balance
        self.email = email
        self.badge_id = badge_id
        self.nfc_id = nfc_id


######################################################################
## Keg class

class Keg(object):
    """a structure class for beers on tap"""
    def __init__(self, keg_id, beer_name, beer_type, abv, ibu, date_tapped, description, start_volume, current_volume, price_per_oz, access, brewery, rating):
        self.keg_id = keg_id
        self.beer_name = beer_name
        self.beer_type = beer_type
        self.abv = abv
        self.ibu = ibu
        self.date_tapped = date_tapped
        self.description = description
        self.start_volume = start_volume
        self.current_volume = current_volume
        self.price_per_oz = price_per_oz
        self.access = access
        self.brewery = brewery
        self.rating = rating
        
        
######################################################################
## Tap class

class Tap(object):
    """a structure class for beers on tap"""
    def __init__(self, tap_number, name, price_per_oz, beerType, abv, ibu, dateBrewed, description, start_volume, access):
        self.tap_number = tap_number
        self.keg_id = keg_id


def build_taps_list(cursor, sql_schema='test', taps_table_name='taps', kegs_table_name='kegs', verbosity=3):#sql_host='192.168.201.36', sql_user='pi', sql_passwd='kegbot', sql_schema='test', taps_table_name='taps', kegs_table_name='kegs', verbosity=3):
    '''Looks for a MySQL database of taps and builds them into the taps_list
        1) Tries to connect to MySQL database
        2) Pulls all rows from taps table (each row is a tap, only 2 columns: tap_number and keg_id)
        3) Iterates through each row, turning the row into a tap object and
            appending it to the taps_list
        Returns the taps_list
    '''
    import MySQLdb
    #taps_table = MySQLdb.connect(host=sql_host, user=sql_user, passwd=sql_passwd, db=sql_schema)
    #taps_cursor = taps_table.cursor()
    taps_kegs = []
    # Fetch all taps in taps_table
    #print "building taps list, return code: ",
    print (cursor.execute("SELECT * FROM %s.%s" % (sql_schema, taps_table_name)))
    # Now to retrieve the taps_results
    taps_results = cursor.fetchall()
    # Each row is a tap
    for i,tap in enumerate(taps_results):
        # make the row into a tap object and append that tap object to the taps_kegs
        taps_kegs.append(int(tap[1])) # end up with list like [2,3,1] where numbers are keg_id's of kegs on tap
        if verbosity>2:
            print "Tap %s has keg_id %s" % ((i+1), int(tap[1]))
    # Have mapping of taps to keg_ids. Now open kegs table and build taps_list
    taps_list = []
    for j in range(len(taps_kegs)):
        # Fetch keg info for keg_id associated with tap[j]
        #print "building kegs list, return code: ",
        print (cursor.execute("SELECT * FROM %s.%s WHERE keg_id = %s" % (sql_schema, kegs_table_name, taps_kegs[j])))
        # Now to retrieve the kegs_results
        keg_result = cursor.fetchall()
        next_keg = Keg(int(keg_result[0][0]), str(keg_result[0][1]), str(keg_result[0][2]), float(keg_result[0][3]), int(keg_result[0][4]), str(keg_result[0][5]), str(keg_result[0][6]), float(keg_result[0][7]), float(keg_result[0][8]), float(keg_result[0][9]), str(keg_result[0][10]), str(keg_result[0][11]), float(keg_result[0][12]))
        if verbosity>2:
            print "Found %s on tap %s" % (str(keg_result[0][1]), (j+1))
        taps_list.append(next_keg)
    if verbosity>1:
        print "Successfully connected to Kegs database"
    return taps_list


#print a table of all User attributes
def print_users_table(sql_schema, sql_table):
    """Prints a formated table of Users and their attributes to the screen"""
    import MySQLdb
    print (cursor.execute("SELECT * FROM %s.%s" %(sql_schema, sql_table)))
    # Now to retrieve the results
    results = cursor.fetchall()
    print "-"*68
    print "| %6s | %20s | %9s |   %15s" % ("User ID", "Name", "Employee?", "Email")
    print "|--------+----------------------+-----------+--------------------------------"
    # Each row is a user with row[0]=user_id, row[1]=first_name, etc...
    for row in results:
        print "| %6s | %20s | %6s    | %31s" % (str(row[0]), (str(row[1]) + ' ' + str(row[2])), str(row[3]), str(row[6]))


#print a table of all Tap attributes
def print_taps_table(tap_list):
    """Prints a formated table of Users and their attributes to the screen"""
    print "-"*115
    print "| %3s | %25s | %6s | %25s | %4s | %4s | %10s | %6s" % ("Tap", "Beer Name", "Cost/oz", "Beer Type", "ABV", "IBUs", "Date Brewed", "Remaining Oz.")
    print "|-----+---------------------------+---------+---------------------------+------+------+-------------+--------------"
    for i,tap in enumerate(tap_list):
        print "| %3s | %25s |   $%.2f | %25s | %4s | %4s | %10s  | %6s" % ((i+1), tap.beer_name, tap.price_per_oz, tap.beer_type, tap.abv, tap.ibu, tap.date_tapped, tap.current_volume)
    print "-"*115
    print


def post_web_message(web_server_ip, action, data):
    """ Sends a post message to the webserver to update a field """
    import requests
    import sys
    requests.post(web_server_ip, { 'action':action, 'data':data})


def new_session(loginTime, user, flow_counters, verbosity=0):
    """ Creates a new 'currentSession' log and writes the date to it

        numTaps - positive integer (number of beer tap lines we're tracking
                verbosity - 1 or True prints out messages for development (default=0)

                Does the following steps:
                2) get/format date & time
                1) delete currentSession.txt
                3) create new currentSession.txt
                4) put date/time as line 1 of currentSession.txt
                5) close currentSession.txt file
                6) reset flow counters

        """
    import os # for modifying session log files
    import datetime # for formatting session time
    import time
    if verbosity>0:
            print "Starting new session\n"
    # 1) Get and format date/time YYYY_MM_DD_hhmmss
    #loginTime = datetime.datetime.now()
    # 2) delete currentSession.txt
    os.chdir('/home/pi') #ensure we're in the right directory
    try:
        if verbosity>1:
            print "Looking for currentSession.txt in: ", str(os.getcwd())
        os.remove("currentSession.txt") #delete previous session log
    except OSError:
        if verbosity>1:
            print "%s -- File not found! We will create it." % loginTime.strftime('%B %d, %Y %I:%M%p')
    time.sleep(0.01) # wait for 10ms
    # 3) create new currentSession.txt
    currSessLog = open("currentSession.txt", "w") #should be blank
    # 4) put date/time as line 1 of currentSession.txt
    currSessLog.write('-'*65)
    currSessLog.write("\nSession Report for %s %s:\n" % (user.first_name, user.last_name))
    currSessLog.write("\t%s\n" % (loginTime.strftime('%B %d, %Y %I:%M%p')))
    currSessLog.write('-'*65)
    currSessLog.write('\n\n')
    # 5) close currentSession.txt file
    currSessLog.close()
    if verbosity>2:
        print "Displaying currentSession.txt on screen:"
        currSessLog = open("currentSession.txt","r")
        print currSessLog.read()
        currSessLog.close()
    time.sleep(0.01) # wait for 10ms, making sure file is there when next fcn looks for it
    # 6) reset tap counters
    for key in flow_counters:
        flow_counters[key] = 0
    if verbosity>0:
        print "Resetting flow counters"
    if verbosity>1:
        print "Flow counters now: ", str(flow_counters)


def email_pour_report(current_user, admins, mail_server, verbosity=0): # will include User object to get email addr from (later)

    # If user has negative balance, attach image of thug with bat and mention something about how I want my money

    """ Creates a new 'currentSession' log and writes the date to it
                Does the following steps:
                1) copies text of currentSession.txt into email body
                2) Sets to, from and subject of email
                3) Send the email
    """
    import os
    import smtplib
    from email.mime.text import MIMEText
    import datetime
    # 1) copies text of currentSession.txt into email body
    try:
        os.chdir('/home/pi') #ensure we're in the right directory
        if verbosity>2:
            print "Looking for currentSession.txt in ", str(os.getcwd())
        currSessLog = open("currentSession.txt", 'r')
        msg = MIMEText(currSessLog.read())
        currSessLog.close()
    except IOError:
        if verbosity>0:
            print "%s -- OHNOES! File not found!" % datetime.datetime.now().strftime('%Y_%m_%d_%H%M%S')
        msg = MIMEText("DEFAULT MESSAGE BECAUSE currentSession.txt WASN'T FOUND")
    # 2) Sets to, from and subject of email
    msgTo = current_user.email
    msgFrom = "Kegbot@nuvation.com"
    msgBcc = admins
    subject = "[KB] Pour Report - " + str(datetime.datetime.now().strftime('%B %d, %Y %I:%M%p'))
    msg['To'] = msgTo
    msg['From'] = msgFrom
    msg['Subject'] = subject
    # 3) Send the email
    try:
        mail_server.sendmail(msgFrom, [msgTo]+msgBcc, msg.as_string())
    except:
        s = smtplib.SMTP('mail.nuvation.com')
        s.sendmail(msgFrom, [msgTo]+msgBcc, msg.as_string())
        s.quit()
    if verbosity>2:
        print 'Pour Report message sent.\n'


def report_bad_login(num_found, badge_id, admins, verbosity):
    """ Emails the admins when the RFID reader detects shenanigans """
    import smtplib
    import string
    import datetime
    TO = admins
    FROM = "Kegbot@nuvation.com"
    SUBJECT = "Bad login at %s" % (datetime.datetime.now().strftime('%B %d, %Y %I:%M%p'))
    text = "%s users with badge ID %s found!" % (num_found, badge_id)
    BODY = string.join((
            "From: %s" % FROM,
            "To: %s" % TO,
            "Subject: %s" % SUBJECT ,
            "",
            text
            ), "\r\n")
    server = smtplib.SMTP('mail.nuvation.com')
    server.sendmail(FROM, TO, BODY)
    server.quit()
    if verbosity>1:
        print 'Bad login report sent to admins.\n'


def reset_flow_counters(flow_counters, verbosity):
    for key in flow_counters:
        flow_counters[key] = 0
    if verbosity>1:
        print "Resetting flow counters"
    if verbosity>2:
        print "Flow counters now: ", str(flow_counters)


def finalize_session(login_time, admins, current_user, flow_counters, tap_list, cts_per_oz, session_timed_out, web_server_ip, open_schema, transactions_table, kegbot_cursor, verbosity=3):
    import os
    import MySQLdb    
    # Open the log file for editing
    try:
        currSessLog = open("currentSession.txt", 'a')
    except IOError:
        if verbosity>1:
            print "%s -- Gr! File not found!"
        # We're not handling this error very well. Something needs to happen besides print...
    new_user_balance = current_user.balance# + increment_user_balance_by
    # Warn user to be more careful if their account timed out
    if session_timed_out == True:
        currSessLog.write('*'*65)
        currSessLog.write("\n*\tYour session timed out due to inactivity. Be sure to\t *")
        currSessLog.write("\n*\tbadge out unless you want to buy everyone a round\t *\n")
        currSessLog.write('*'*65)
        currSessLog.write('\n\n')
    # Check which taps they poured from and update SQL tables & logfile
    for i,tap in enumerate(flow_counters):
        # Check to see if beer was poured from this tap
        if flow_counters[tap] > 20: # Flow counter naturally spins when valve is opened, so ignore ~20 counts
            ounces = flow_counters[tap] / cts_per_oz
            pour_cost = round(ounces*tap_list[i].price_per_oz, 2)
            new_user_balance = new_user_balance - pour_cost
            if new_user_balance < 0 and current_user.user_id == 1:
                new_user_balance = 0
            new_keg_volume = round((tap_list[i].current_volume - ounces),2)
            currSessLog.write("\n---------- Tap %s:  %s ----------\n" % ((i+1), tap_list[i].beer_name))
            # Update keg's current_volume
            update_keg_volume(tap_list[i], new_keg_volume, kegbot_cursor, verbosity)
            currSessLog.write("\tAmount poured: %.2foz\n" % ounces)
            currSessLog.write("\tAmount remaining in keg: %.0f oz. (%.0f pints)\n" % (new_keg_volume, new_keg_volume/20))
            currSessLog.write("\tPrice per ounce for this beer: $%.2f\n" % tap_list[i].price_per_oz)
            currSessLog.write("\tPour Total: $%.2f\n" % pour_cost)
            # Write transaction data to SQL
            kegbot_cursor.execute("""INSERT INTO `%s`.`%s` (`session_time`, `user_id`, `keg_id`, `oz_poured`)
                VALUES ('%s', '%s', '%s', '%s');""" % (open_schema, transactions_table, login_time, current_user.user_id, tap_list[i].keg_id, round(ounces,2)))
            # Check if keg is getting low and email admins if so
            
    ### ADD BACK FCN TO EMAIL ADMINS WHEN KEG IS LOW
            
            if new_keg_volume <= 256: # Keg has less than 2 gallons remaining
            #    email_admins_keg_low(admins, "[KEG LOW]", (i+1), tap_list[i], verbosity)
                currSessLog.write("\tWARNING! KEG HAS %.1f PINTS (%s OUNCES) REMAINING!\n" % (new_keg_volume/20, new_keg_volume))
    # Update user's balance if user is not guest (guest balance has already been adjusted)
    if current_user.user_id > 1:
        update_user_balance(current_user, new_user_balance, kegbot_cursor, verbosity)
        currSessLog.write("\n\nYour remaining balance is: $%.2f\n\n" % new_user_balance)
    else: # guest account
        update_user_balance(current_user, current_user.balance, kegbot_cursor, verbosity)
        currSessLog.write("\n\nRemaining balance on the guest account: $%.2f\n\n" % current_user.balance)
    currSessLog.write("\nCheers!\n")
    currSessLog.close()
    if verbosity>1:
        currSessLog = open("currentSession.txt", 'r')
        print currSessLog.read()
        currSessLog.close()
    # Update web interface dynamic text
    post_web_message(web_server_ip, 'logout', current_user.user_id)
    

def update_keg_volume(tap, new_keg_volume, cursor, verbosity=3):
    import MySQLdb
    try:
        #print "update_keg_volume, return code: ",
        print (cursor.execute("UPDATE kegs SET current_volume=%s WHERE keg_id=%s;" % (new_keg_volume, tap.keg_id)))
    except:
        print "couldn't run cursor.execute command in update_keg_volume"
    if verbosity>2:
        print "Updating keg volume to %.2f oz." % (new_keg_volume)


def update_user_balance(current_user, new_balance, cursor, verbosity=3):
    import MySQLdb
    
    try:
        #print "update_user_balance, return code: ",
        print (cursor.execute("UPDATE users SET balance=%.2f WHERE user_id=%s;" % (new_balance, current_user.user_id)))
    except:
        print "couldn't run cursor.execute command in update_user_balance"
    if verbosity>2:
        print "%s's balance set to $%.2f" % (current_user.first_name, new_balance)



## Make functions executable as scripts
if __name__ == "__main__":
    verbosity = 1
    new_session(verbosity)